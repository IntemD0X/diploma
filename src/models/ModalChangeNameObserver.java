package models;

public interface ModalChangeNameObserver {
    void update(String text);
}
